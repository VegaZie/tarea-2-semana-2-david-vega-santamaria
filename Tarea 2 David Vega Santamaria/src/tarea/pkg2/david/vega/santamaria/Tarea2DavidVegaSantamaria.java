/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea.pkg2.david.vega.santamaria;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class Tarea2DavidVegaSantamaria {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean i = true;
        while (i) {
            System.out.println("-------------------------------");
            System.out.println("Bienvenido al menú de las tarea 2 de la semana 2");
            System.out.println("1- Edad Actual");
            System.out.println("2- Salarios");
            System.out.println("3- Contador hasta 20");
            System.out.println("4- Promedios de notas");
            System.out.println("5- Salir");
            System.out.println("-------------------------------");
            System.out.println("Dígite la opción que desa utilizar: ");
            Scanner var = new Scanner(System.in);
            int opcion = var.nextInt();
            switch (opcion){
                    case 1:
                        EdadActual.edadactual();
                        i = true;
                        break;
                    case 2:
                        Salarios.salarios();
                        i = true;
                        break;
                    case 3:
                        contador_20.contador();
                        i = true;
                        break;
                    case 4: 
                        PromedioNotas.promedios();
                        i = true;
                        break;
                    case 5:
                        i = false;
                        break;
            }
        }
        System.out.println("Gracias por revisar las tarea 2 de la semana 2 <:-)");

    }
    
}
